<?php

/**
 * @file
 * Contains \Drupal\apiservices\Exception\EndpointNotFoundException.
 */

namespace Drupal\apiservices\Exception;

/**
 * An exception thrown when a requested resource could not be found.
 */
class EndpointNotFoundException extends EndpointRequestException {}
