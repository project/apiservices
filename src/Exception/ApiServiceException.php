<?php

/**
 * @file
 * Contains \Drupal\apiservices\Exception\ApiServiceException.
 */

namespace Drupal\apiservices\Exception;

/**
 * A generic exception thrown when an unknown error has occured.
 */
class ApiServiceException extends \Exception {}
